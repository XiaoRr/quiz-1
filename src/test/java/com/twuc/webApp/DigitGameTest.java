package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Field;

public class DigitGameTest {

    @Test
    public void guessTest() throws NoSuchFieldException, IllegalAccessException {

        DigitGame digitGame = new DigitGame();
        Field field = DigitGame.class.getDeclaredField("answer");
        field.setAccessible(true);
        field.set(digitGame,"1234");

        assertEquals("{ \"hint\": \"4A0B\", \"correct\": true }",digitGame.guessAnswer("1234"));
        assertEquals("{ \"hint\": \"0A4B\", \"correct\": false }",digitGame.guessAnswer("4321"));
        assertEquals("{ \"hint\": \"1A3B\", \"correct\": false }",digitGame.guessAnswer("1423"));
        assertEquals("{ \"hint\": \"2A2B\", \"correct\": false }",digitGame.guessAnswer("1324"));
        assertEquals("{ \"hint\": \"0A0B\", \"correct\": false }",digitGame.guessAnswer("7890"));
        assertEquals("{ \"hint\": \"0A1B\", \"correct\": false }",digitGame.guessAnswer("3890"));

    }
}

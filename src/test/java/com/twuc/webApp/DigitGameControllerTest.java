package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class DigitGameControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Test
    public void startGameTest() throws Exception {
        mockMvc.perform(post("/api/games")).andExpect(status().is(201));
    }

    @Test
    public void getIdTest() throws Exception {
        mockMvc.perform(post("/api/games")).andExpect(status().is(201));
    }

    @Test
    public void getStatus() throws Exception {
        mockMvc.perform(get("/api/games/1")).andExpect(content().contentType(MediaType.APPLICATION_JSON));
        //.andExpect(jsonPath("$.answer").value(org.hamcrest.Matchers.is("\\d+")))
       //         .andExpect(jsonPath("$.id"),value(org.hamcrest.Matchers.contains("\\d+")));

    }
    @Test
    public void guessNum() throws Exception {
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.patch("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"answer\": 1234 }");
        mockMvc.perform(builder).andExpect(status().is(201));
        //.andExpect(content().string("aaa"));
    }

    @Test
    public void guessWrongNum() throws Exception {
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.patch("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"answer\": 12345 }");
        mockMvc.perform(builder).andExpect(status().is(400));
        builder.content("{ \"answer\": 1231 }" );
        mockMvc.perform(builder).andExpect(status().is(400));
        builder.content("{ \"answer\": 1234 }");
        mockMvc.perform(builder).andExpect(status().is(201));
        builder.content("{ \"answer\": 1111 }");
        mockMvc.perform(builder).andExpect(status().is(400));
    }

    @Test
    public void invalidId() throws Exception {
        mockMvc.perform(get("/api/games/100000")).andExpect(status().is(404));
        mockMvc.perform(get("/api/games/-1")).andExpect(status().is(400));
        mockMvc.perform(get("/api/games/abc")).andExpect(status().is(400));
    }

    @Test
    public void mutipleGameTest()throws Exception {
        for(int i=0;i<10;i++){
            mockMvc.perform(post("/api/games"));
        }
        mockMvc.perform(get("/api/games/10")).andExpect(status().is(201));
        mockMvc.perform(get("/api/games/12000000")).andExpect(status().is(404));

    }

}

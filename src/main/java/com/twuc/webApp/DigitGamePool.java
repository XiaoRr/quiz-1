package com.twuc.webApp;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DigitGamePool {
    private Map<Integer,DigitGame> gamepool;
    private int id;
    DigitGamePool() {
        gamepool = new HashMap<Integer, DigitGame>();
        id = 1;
    }

    public int addGame(){
        //AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext("com.twuc.webApp");
        //DigitGame digitGame = ctx.getBean(DigitGame.class);
        DigitGame digitGame = new DigitGame();
        gamepool.put(id,digitGame);
        return id++;
    }

    public DigitGame getGame(int i){
        return gamepool.get(i);
    }
}

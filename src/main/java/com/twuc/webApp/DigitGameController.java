package com.twuc.webApp;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class DigitGameController {

    private DigitGamePool digitGamePool;

    DigitGameController(DigitGamePool digitGamePool){
        this.digitGamePool = digitGamePool;
        this.digitGamePool.addGame();
   }
    @PostMapping(value="/api/games")
    public ResponseEntity<String> gameStart(){
        int id = this.digitGamePool.addGame();
        return ResponseEntity.status(201).header("locationHeader","/api/games/"+id).build();
    }

    @RequestMapping(value="/api/games/{id}")
    public ResponseEntity<String> gameStart(@PathVariable String id){
        if(!isNum(id)){
            return ResponseEntity.status(400).build();
        }
        DigitGame digitGame = digitGamePool.getGame(Integer.parseInt(id));
        if(digitGame == null){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(201)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                    String.format( "{ \"id\": %s, \"answer\": \"%s\"}",id,digitGame.getAnswer())
                );
    }

    @PatchMapping(value="/api/games/{id}")
    public ResponseEntity<String> guessNum(@PathVariable String id,@RequestBody Guess guess){
        if(!isNum(id)){
            return ResponseEntity.status(400).build();
        }
        DigitGame digitGame = digitGamePool.getGame(Integer.parseInt(id));
        if(digitGame == null){
            return ResponseEntity.status(404).build();
        }
        if(!guess.isCorrect()){
            return ResponseEntity.status(400).build();
        }
        return ResponseEntity.status(201)
                .contentType(MediaType.APPLICATION_JSON)
                .body(
                        digitGame.guessAnswer(guess.getAnswer())
                );

    }

    private boolean isNum(String id){
        Pattern pattern = Pattern.compile("\\d+");
        Matcher ans = pattern.matcher(id);
        if( ans.matches() ){
            return true;
        }
        return false;
    }

}

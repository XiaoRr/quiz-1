package com.twuc.webApp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Guess {
    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    private String answer;
    public Guess(){

    }

    public boolean isCorrect(){
        Pattern pattern = Pattern.compile("^(?!.*(.).*\\1)\\d{4}");
        Matcher ans = pattern.matcher(answer);
        if( ans.matches() ){
            return true;
        }
        return false;
    }
}

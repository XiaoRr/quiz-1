package com.twuc.webApp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Scope("prototype")
public class DigitGame {
    private String answer;
    public DigitGame(){

        List<Integer> numbers = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            numbers.add(i);
        }

        Collections.shuffle(numbers);
        answer = "";
        for(int i = 0; i < 4; i++){
            answer += numbers.get(i).toString();
        }
    }

    public String getAnswer(){
        return answer;
    }

    public String guessAnswer(String guess) {
        int countA = 0,countB = 0;
        for(int i=0;i<4;i++){
            if(guess.charAt(i) == answer.charAt(i))
                countA++;
        }
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                if(i==j)continue;
                if(guess.charAt(i) == answer.charAt(j)){
                    countB++;
                }
            }
        }
        return String.format("{ \"hint\": \"%dA%dB\", \"correct\": %s }",countA,countB,countA==4?"true":"false");
    }

}
